package calculatuindemnizacion.pro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class webServiceJsonConnector {
	public String sendJSONtoURL(String targetURL,JSONObject userData ){
		//Establish URL connection*************************************************************
		String response = "";
		  URL url = null;
		try {
			url = new URL(targetURL);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		  HttpURLConnection conn = null;
		  try {
		    conn = (HttpURLConnection) url.openConnection();
		    try {
		      conn.setRequestMethod("POST"); //use post method
		      conn.setDoOutput(true); //we will send stuff
		      conn.setDoInput(true); //we want feedback
		      conn.setUseCaches(false); //no caches
		      conn.setAllowUserInteraction(false);
		      conn.setRequestProperty("Content-Type","text/xml");
		    } 
		    catch (ProtocolException e) {
		    }

		    // Open a stream which can write to the URL******************************************
		    OutputStream out = conn.getOutputStream();
		    try {
		      OutputStreamWriter wr = new OutputStreamWriter(out);
		      wr.write(userData.toString()); //userData is my JSON object containing the api commands
		      wr.flush();
		      wr.close();
		    }
		    catch (IOException e) {
		    }
		    finally { //in this case, we are ensured to close the output stream
		      if (out != null)
		        out.close();
		    }

		    // Open a stream which can read the server response*********************************
		    InputStream in = conn.getInputStream();
		    try {
		      BufferedReader rd  = new BufferedReader(new InputStreamReader(in));
		      String responseSingle = "";
		      while ((responseSingle = rd.readLine()) != null) {
		        response = response + responseSingle;
		      }
		      rd.close(); //close the reader
		      //println("The server response is " + response);
		    }
		    catch (IOException e) {
		    }
		    finally {  //in this case, we are ensured to close the input stream
		      if (in != null)
		        in.close();
		    }
		  }
		  catch (IOException e) {
		  } 
		  finally {  //in this case, we are ensured to close the connection itself
		    if (conn != null)
		      conn.disconnect();
		  }
		  //Done communicating with server******************************************************
		  return response;
	  }
	
	
	public static JSONObject getJSONfromURL(String url){
	     InputStream is = null;
	     String result = "";
	     JSONObject json = null;
	      try{
	         HttpClient httpclient = new DefaultHttpClient();
	         HttpPost httppost = new HttpPost(url);
	         HttpResponse response = httpclient.execute(httppost);
	         HttpEntity entity = response.getEntity();
	         is = entity.getContent();
	     }catch(Exception e){}

	      try{
	         BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	         StringBuilder sb = new StringBuilder();
	         String line = null;
	         while ((line = reader.readLine()) != null) {
	             sb.append(line + "\n");
	         }
	         is.close();
	         result=sb.toString();
	     } catch(Exception e){}

	     try{
	         json = new JSONObject(result);
	     }catch(JSONException e){}

	      return json;
	 }
}
