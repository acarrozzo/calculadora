package calculatuindemnizacion.pro;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class webServiceSoapConnector {

	public String getResult(String[]  userData) {
				
		String webServiceResult = null;
		SoapObject rpc = new SoapObject("http://test.accidente.pro", "soap_reciever"); //funcion a llamar del webservice
		//orden de los parametros para la llamada a la funcion del webservice  soap_reciever($email,$escala,$anyo,$puntos,$diashospital,$diasimpeditivos,$diasnoimpeditivos,$ingresos)
		rpc.addProperty("email" , userData[0]);
		rpc.addProperty("escala" , userData[1]);
		rpc.addProperty("anyo" , userData[2]);
		rpc.addProperty("puntos" , userData[3]);
		rpc.addProperty("diashospital" , userData[4]);
		rpc.addProperty("diasimpeditivos" , userData[5]);
		rpc.addProperty("diasnoimpeditivos" , userData[6]);
		rpc.addProperty("ingresos" , userData[7]);
		SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		soapEnvelope.bodyOut = rpc;
		soapEnvelope.dotNet = false;
		soapEnvelope.encodingStyle = SoapSerializationEnvelope.XSD;
		
		HttpTransportSE androidHttpTransport = null;
		String connection = "http://test.accidente.pro/wp-content/themes/nevada/servicio.php";//pagina que recoge la conexi�n soap
		try{
			androidHttpTransport = new HttpTransportSE(connection);
			androidHttpTransport.debug = true;
			androidHttpTransport.call("http://test.accidente.pro/wp-content/themes/nevada/Calculator", soapEnvelope); //nombre de la clase que implementa el servicio
			webServiceResult = soapEnvelope.getResponse().toString();
		} catch(Exception e) {
			//
			//webServiceResult = e.getMessage();
			webServiceResult = "Algo ha fallado en la conexi�n con el servicio web. Por favor, intentelo m�s tarde";
		}
		
		return webServiceResult;
	}
	
}
