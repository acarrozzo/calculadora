package calculatuindemnizacion.pro;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HorizontalSpinner extends LinearLayout
{
	private TextView label;
	private EditText textField;
    private Button rightButton;
    private Button leftButton;
    private int minValue = 1;
    private int maxValue = 1;
    private int stepSize = 1;
    
    public HorizontalSpinner(Context context) {
       super(context);
       init();
    }

    public HorizontalSpinner(Context context, AttributeSet attrs) {
    	super(context, attrs);
    	init();
     }
    private void init(){
    	String inflaterService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(inflaterService);
        inflater.inflate(R.layout.horizontal_spinner, this, true);
     
        label = (TextView)findViewById(R.id.label);
        textField = (EditText)findViewById(R.id.field);
        leftButton = (Button)findViewById(R.id.leftButton);
        rightButton = (Button)findViewById(R.id.rightButton);
        
        leftButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				int value = Integer.parseInt(textField.getText().toString());
				value -=stepSize;
				textField.setText(String.valueOf(value));
				rightButton.setEnabled(true);
				if(value<= minValue){
					leftButton.setEnabled(false);
				}
				
			}
		});
        rightButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				int value = Integer.parseInt(textField.getText().toString());
				value +=stepSize;
				textField.setText(String.valueOf(value));
				leftButton.setEnabled(true);
				if(value>= maxValue){
					rightButton.setEnabled(false);
				}
			}
		});
     
    }
    public void setLabel(String text)
    {
    	label.setText(text);
    }
    
    public void setMinValue(int min)
    {
    	this.minValue = min;
    }
    
    public void setMaxValue(int max)
    {
    	this.maxValue = max;
    }
    public void setStep(int step)
    {
    	this.stepSize = step;
    }    
    
    
    public String getFieldValue(){
    	String value = "";
		value = textField.getText().toString();
		return value;
    }
    public void setDefaultValue (String value){
    	textField.setText(value);
    }

}