package calculatuindemnizacion.pro;

import java.util.LinkedList;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


public class MainActivity extends Activity {

	private Context actualContext;
	private Button calculateButton;
	private String[] userData;
	private JSONObject jsonData;
	private HorizontalSpinner yearSpinner;
	private HorizontalSpinner pointSpinner;
	private HorizontalSpinner hospitalDaysSpinner;
	private HorizontalSpinner impeditiveHospitalDaysSpinner;
	private HorizontalSpinner noImpeditiveHospitalDaysSpinner;
	private HorizontalSpinner incomeSpinner;
	private Spinner ageSpinner; 
	private ProgressDialog progressDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		actualContext = this;
		
		ageSpinner = (Spinner) findViewById(R.id.ageSpinner);
		
		LinkedList<Age> yearList = new LinkedList<Age>();
		yearList.add(new Age(0, "hasta 18"));
		yearList.add(new Age(1, "18 - 20"));
		yearList.add(new Age(2, "21 - 40"));
		yearList.add(new Age(3, "41 - 55"));
		yearList.add(new Age(4, "56 - 65"));
		yearList.add(new Age(5, "65+"));
		
		ArrayAdapter<Age> spinner_adapter = new ArrayAdapter<Age>(this, android.R.layout.simple_spinner_item, yearList);
		spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ageSpinner.setAdapter(spinner_adapter);

        yearSpinner = (HorizontalSpinner) findViewById(R.id.yearSpinner);
        yearSpinner.setMinValue(2007);
        yearSpinner.setMaxValue(2013);
        yearSpinner.setStep(1);
        yearSpinner.setDefaultValue("2012");
        yearSpinner.setLabel("A�o del accidente");
        
        pointSpinner = (HorizontalSpinner) findViewById(R.id.pointSpinner);
        pointSpinner.setMinValue(1);
        pointSpinner.setMaxValue(100);
        pointSpinner.setStep(1);
        pointSpinner.setDefaultValue("1");
        pointSpinner.setLabel("Puntos");
        
        hospitalDaysSpinner = (HorizontalSpinner) findViewById(R.id.hospitalDaysSpinner);
        hospitalDaysSpinner.setMinValue(1);
        hospitalDaysSpinner.setMaxValue(365);
        hospitalDaysSpinner.setStep(1);
        hospitalDaysSpinner.setDefaultValue("1");
        hospitalDaysSpinner.setLabel("Hospitalarios");
        
        impeditiveHospitalDaysSpinner = (HorizontalSpinner) findViewById(R.id.impeditiveHospitalDaysSpinner);
        impeditiveHospitalDaysSpinner.setMinValue(1);
        impeditiveHospitalDaysSpinner.setMaxValue(365);
        impeditiveHospitalDaysSpinner.setStep(1);
        impeditiveHospitalDaysSpinner.setDefaultValue("1");
        impeditiveHospitalDaysSpinner.setLabel("Impeditivos");
        
        noImpeditiveHospitalDaysSpinner = (HorizontalSpinner) findViewById(R.id.noImpeditiveHospitalDaysSpinner);
        noImpeditiveHospitalDaysSpinner.setMinValue(1);
        noImpeditiveHospitalDaysSpinner.setMaxValue(365);
        noImpeditiveHospitalDaysSpinner.setStep(1);
        noImpeditiveHospitalDaysSpinner.setDefaultValue("1");
        noImpeditiveHospitalDaysSpinner.setLabel("No impeditivos");
        
        incomeSpinner = (HorizontalSpinner) findViewById(R.id.incomeSpinner);
        incomeSpinner.setMinValue(0);
        incomeSpinner.setMaxValue(99000);
        incomeSpinner.setStep(1000);
        incomeSpinner.setDefaultValue("20000");
        incomeSpinner.setLabel("Ingresos");

		calculateButton = (Button) findViewById(R.id.calculateButton);
		calculateButton.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				showPreloader();
				new JsonConnectionDialog().execute();
			}
		});
	}
	private void showResult(String result){
		Toast.makeText(actualContext, result,Toast.LENGTH_LONG).show();
	}
	private void showPreloader(){
		progressDialog = ProgressDialog.show(MainActivity.this, "","Loading. Please wait...", true);
	}
	private String[] collectData(){
		userData = new String[8];
		userData[0] = yearSpinner.getFieldValue();
		userData[1] = String.valueOf(ageSpinner.getSelectedItemPosition());
		userData[2] = pointSpinner.getFieldValue();
		userData[3] = hospitalDaysSpinner.getFieldValue();
		userData[4] = impeditiveHospitalDaysSpinner.getFieldValue();
		userData[5] = noImpeditiveHospitalDaysSpinner.getFieldValue();
		userData[6] = incomeSpinner.getFieldValue();
		userData[7] = "jonnyjava.net@gmail.com";
		return userData;
	}
	private String calculate(String[] userData){
		jsonData = new JSONObject(); 
		String result;
		//json webservice function definition. The paramenter order is important.
		//soap_reciever($email,$escala,$anyo,$puntos,$diashospital,$diasimpeditivos,$diasnoimpeditivos,$ingresos)
	    try {
	        jsonData.put("email", userData[7]);
	        jsonData.put("escala", userData[1]);
	        jsonData.put("anyo", userData[0]);
	        jsonData.put("puntos", userData[2]);
	        jsonData.put("diashospital", userData[3]);
	        jsonData.put("diasimpeditivos", userData[4]);
	        jsonData.put("diasnoimpeditivos", userData[5]);
	        jsonData.put("ingresos", userData[6]);
	    } catch (JSONException e) {
	        e.printStackTrace();
	    }
	    webServiceJsonConnector dataLoader = new webServiceJsonConnector();
		result = dataLoader.sendJSONtoURL("http://test.accidente.pro/wp-content/themes/nevada/jsonService.php",jsonData);
		return result;
	}
	
	class JsonConnectionDialog extends AsyncTask<String, Void, Object> {
		private String result; 
		protected String doInBackground(String ... args){
			result = calculate(collectData());
			return result;
		}
	
		protected void onPostExecute (Object result){
			progressDialog.dismiss();
			MainActivity.this.showResult(String.valueOf(result));
			super.onPostExecute(result);
		}
   }
}